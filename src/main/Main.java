package main;

import java.util.Scanner;
import java.util.Set;

import animals.*;
import animals.Animal.IAnimalDeadListener;
import interfaceZoo.Jumpable;
import interfaceZoo.Soundable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main implements IAnimalDeadListener {
	public static final Scanner sc = new Scanner(System.in);
	private ExtensibleCage<Mammal> mammalCage;
	private ExtensibleCage<Bird> birdCage;
	private ExtensibleCage<Herbivore> herbivoreCage;
	Map<String, ExtensibleCage<? extends Animal>> cages = new HashMap<>();

	private Main() {
		cages.put(Mammal.class.getSimpleName(), new ExtensibleCage<Mammal>());
		cages.put(Bird.class.getSimpleName(), new ExtensibleCage<Bird>());
		cages.put(Herbivore.class.getSimpleName(), new ExtensibleCage<Herbivore>());
		boolean userInput = true;
		while (userInput) {
			System.out.println(
					"1) ����  \n 2) ��������� �������� \n 3) ������� �������� \n 4) �������� ������ �� ������� ����� �������� \n 5) ������� �������� \n 6) �����");
			String answer = sc.nextLine();
			switch (inputValidation(answer)) {
			case 1: {
				showAnimalInfo();
				break;
			}
			case 2: {
				feedAnimal();
				break;
			}
			case 3: {
				System.out.println("�������� ���������...");
				Animal ani = createAnimal();
				if (ani != null) {
					if (ani instanceof Predator) {
						((ExtensibleCage<Mammal>) cages.get(Mammal.class.getSimpleName())).addAnimal((Predator) ani);
						System.out.println(
								"������ ������ ��� mammal " + cages.get(Mammal.class.getSimpleName()).cage.size());

					} else if (ani instanceof Bird) {
						ExtensibleCage cage = (ExtensibleCage<Bird>) cages.get(Bird.class.getSimpleName());
						cage.addAnimal((Bird) ani);
						System.out
								.println("����� ������ ��� ���� " + cages.get(Bird.class.getSimpleName()).cage.size());
					} else if (ani instanceof Herbivore) {
						Herbivore herbivore = (Herbivore) ani;
						if (Math.random() >= .5) {
							((ExtensibleCage<Herbivore>) cages.get(Herbivore.class.getSimpleName()))
									.addAnimal((Herbivore) ani);
							System.out.println("������ ������ ��� ���������� "
									+ cages.get(Herbivore.class.getSimpleName()).cage.size());
						} else {
							((ExtensibleCage<Mammal>) cages.get(Mammal.class.getSimpleName())).addAnimal((Mammal) ani);
							System.out.println(
									"������ ������ ��� mammal " + cages.get(Mammal.class.getSimpleName()).cage.size());
						}
					}
				}
				break;
			}
			case 4: {
				jumpAll();

				break;
			}
			case 5: {
				removeAnimal();
				break;
			}
			case 6: {
				userInput = false;
				System.out.println("����� ���������");
			}
			}

		}

	}

	public static void main(String[] args) {

		new Main();
		sc.close();
	}

	public static void displayAnimals(Animal[] ani) {
		for (int i = 0; i < ani.length; i++)
			System.out.println(ani[i].toString());
	}

	public void SoundAll(Soundable[] cat) {
		for (int i = 0; i < cat.length; i++) {

			cat[i].makeSound();
		}
	}

	public void jumpAll() {
		if (cages.get(Mammal.class.getSimpleName()).cage.isEmpty()
				&& cages.get(Bird.class.getSimpleName()).cage.isEmpty()
				&& cages.get(Herbivore.class.getSimpleName()).cage.isEmpty()) {
			System.out.println("Error///In competation no one play!!");
			return;
		}
		int maxElement = 0;
		double maxLenght = 0;
		if (!cages.get(Mammal.class.getSimpleName()).cage.isEmpty()) {
			for (int i = 0; i < cages.get(Mammal.class.getSimpleName()).cage.size(); i++) {
				if (cages.get(Mammal.class.getSimpleName()).cage.get(i).jump() > maxLenght) {
					maxLenght = cages.get(Mammal.class.getSimpleName()).cage.get(i).jump();
					maxElement = i;
				}
			}
			System.out.println("������ ���� in cageMammal ������� "
					+ cages.get(Mammal.class.getSimpleName()).cage.get(maxElement).toString() + " jump = " + maxLenght);
		} else {
			System.out.println("Among animals from cageMammal no one animal");
		}
		if (!cages.get(Bird.class.getSimpleName()).cage.isEmpty()) {
			for (int i = 0; i < cages.get(Bird.class.getSimpleName()).cage.size(); i++) {
				if (cages.get(Bird.class.getSimpleName()).cage.get(i).jump() > maxLenght) {
					maxLenght = cages.get(Bird.class.getSimpleName()).cage.get(i).jump();
					maxElement = i;
				}
			}
			System.out.println("������ ���� in cageBird ������� "
					+ cages.get(Bird.class.getSimpleName()).cage.get(maxElement).toString() + " jump = " + maxLenght);
		} else {
			System.out.println("Among animals from cageBird no one animal");
		}
		if (!cages.get(Herbivore.class.getSimpleName()).cage.isEmpty()) {
			for (int i = 0; i < cages.get(Herbivore.class.getSimpleName()).cage.size(); i++) {
				if (cages.get(Herbivore.class.getSimpleName()).cage.get(i).jump() > maxLenght) {
					maxLenght = cages.get(Herbivore.class.getSimpleName()).cage.get(i).jump();
					maxElement = i;
				}
			}
			System.out.println("������ ���� in cageHerbivore ������� "
					+ cages.get(Herbivore.class.getSimpleName()).cage.get(maxElement).toString() + " jump = "
					+ maxLenght);
		} else {
			System.out.println("Among animals from cageHerbivore no one animal");
		}
	}

	public static int inputValidation(String answer) {

		if (answer.equals("1")) {
			return 1;
		} else if (answer.equals("2")) {
			return 2;
		} else if (answer.equals("3")) {
			return 3;
		} else if (answer.equals("4")) {
			return 4;
		} else if (answer.equals("5")) {
			return 5;
		} else {
			return 6;
		}

	}

	public Animal createAnimal() {
		System.out.println("1) ����, 2) ��� 3) ������ 4) ������");
		String answer = sc.nextLine();
		boolean userInput = true;
		while (userInput) {
			if (inputValidation(answer) == 1) {
				System.out.println("�������� �����...");
				System.out.println("���� ������");
				String nickName = sc.nextLine();
				System.out.println("���� �������");
				double size = sc.nextDouble();
				sc.nextLine();
				Animal wolf = new ForestWolf(nickName, size);
				userInput = false;
				wolf.setAnimalDeadListener(this);
				return wolf;
			} else if (inputValidation(answer) == 2) {
				System.out.println("�������� ����...");
				System.out.println("���� ������");
				String nickName = sc.nextLine();
				System.out.println("���� �������");
				double size = sc.nextDouble();
				sc.nextLine();
				Animal cat = new DomesticCat(nickName, size);
				userInput = false;
				cat.setAnimalDeadListener(this);
				return cat;
			} else if (inputValidation(answer) == 3) {
				System.out.println("�������� ������...");
				System.out.println("���� ������");
				String nickName = sc.nextLine();
				System.out.println("���� �������");
				double size = sc.nextDouble();
				sc.nextLine();
				Animal cat = new Raven(nickName, size);
				userInput = false;
				cat.setAnimalDeadListener(this);
				return cat;
			} else if (inputValidation(answer) == 4) {
				System.out.println("�������� �������...");
				System.out.println("���� ������");
				String nickName = sc.nextLine();
				System.out.println("���� �������");
				double size = sc.nextDouble();
				sc.nextLine();
				Animal rabbit = new Rabbit(nickName, size);
				userInput = false;
				rabbit.setAnimalDeadListener(this);
				return rabbit;
			} else {
				userInput = false;
				System.out.println("Error");
			}

		}
		return null;
	}

	public void showAnimalInfo() {
		StringBuilder sb = new StringBuilder();

		Set<String> keys = cages.keySet();
		for (String key : keys) {
			sb.append(cages.get(key).toString());
			sb.append("\n");
		}
		System.out.println(sb.toString());
		dead();
	}

	private void dead() {
		for (int i = 0; i < cages.get("Mammal").cage.size(); i++) {
			if (cages.get("Mammal").cage.get(i).getFill() < 0) {
				onAnimalDead(cages.get("Mammal").cage.get(i));
				cages.get("Mammal").removeAnimal(i);
			}
		}
		for (int i = 0; i < cages.get("Bird").cage.size(); i++) {
			if (cages.get("Bird").cage.get(i).getFill() < 0) {
				onAnimalDead(cages.get("Bird").cage.get(i));
				cages.get("Bird").removeAnimal(i);
			}
		}
		for (int i = 0; i < cages.get("Herbivore").cage.size(); i++) {
			if (cages.get("Herbivore").cage.get(i).getFill() < 0) {
				onAnimalDead(cages.get("Herbivore").cage.get(i));
				cages.get("Herbivore").removeAnimal(i);
			}
		}
	}

	public void feedAnimal() {
		double countOfFood;
		for (String key : cages.keySet()) {
			System.out.println("Size of " + key + " " + cages.get(key).cage.size());
		}
		System.out.println("\nFrom which cage you want to feed animal? \n1-mammal \t2-bird \t3-herbivore");
		int numberCage = sc.nextInt();
		int numberAnimal;
		switch (numberCage) {
		case 1:
			if (cages.get(Mammal.class.getSimpleName()).cage.size() == 0) {
				return;
			} else {
				System.out.println("\nWhich animal you want to feed? From 1 to "
						+ cages.get(Mammal.class.getSimpleName()).cage.size());
				numberAnimal = sc.nextInt() - 1;
				if (numberAnimal + 1 > cages.get(Mammal.class.getSimpleName()).cage.size()) {
					return;
				}
				System.out.println("\nHow much you want to feed the mammal");
				countOfFood = sc.nextDouble();
				sc.nextLine();
				cages.get(Mammal.class.getSimpleName()).cage.get(numberAnimal).feed(countOfFood);
			}
			break;
		case 2:
			if (cages.get(Bird.class.getSimpleName()).cage.size() == 0) {
				return;
			} else {
				System.out.println("\nWhich animal you want to feed? From 1 to "
						+ cages.get(Bird.class.getSimpleName()).cage.size());
				numberAnimal = sc.nextInt() - 1;
				if (numberAnimal + 1 > cages.get(Bird.class.getSimpleName()).cage.size()) {
					return;
				}
				System.out.println("\nHow much you want to feed the bird");
				countOfFood = sc.nextDouble();
				sc.nextLine();
				cages.get(Bird.class.getSimpleName()).cage.get(numberAnimal).feed(countOfFood);
			}
			break;
		case 3:
			if (cages.get(Herbivore.class.getSimpleName()).cage.isEmpty()) {
				return;
			} else {
				System.out.println("\nWhich animal you want to feed? From 1 to "
						+ cages.get(Herbivore.class.getSimpleName()).cage.size());
				numberAnimal = sc.nextInt() - 1;
				if (numberAnimal + 1 > cages.get(Herbivore.class.getSimpleName()).cage.size()) {
					return;
				}
				System.out.println("\nHow much you want to feed the herbivore");
				countOfFood = sc.nextDouble();
				sc.nextLine();
				cages.get(Herbivore.class.getSimpleName()).cage.get(numberAnimal).feed(countOfFood);
			}
			break;
		}
		showAnimalInfo();
	}

	public void removeAnimal() {

		System.out.println("���� �������: 1-��������, 2-�����, 3-�������?");

		int indRemoveAnimal;
		int numberCage = sc.nextInt();
		switch (numberCage) {
		case 1:

			if (cages.get(Mammal.class.getSimpleName()).cage.size() == 0) {
				System.out.println("Error");
				return;
			} else {
				System.out.println(
						"����� �������� �������? � 1 �� " + cages.get(Mammal.class.getSimpleName()).cage.size());
				indRemoveAnimal = sc.nextInt() - 1;
				sc.nextLine();
				if (indRemoveAnimal + 1 > cages.get(Mammal.class.getSimpleName()).cage.size()) {
					return;
				}
				cages.get(Mammal.class.getSimpleName()).removeAnimal(indRemoveAnimal);
			}

			System.out.println("����� ������ ������ " + cages.get(Mammal.class.getSimpleName()).cage.size());
			break;
		case 2:
			if (cages.get(Bird.class.getSimpleName()).cage.size() == 0) {
				System.out.println("Error");
				return;
			} else {
				System.out
						.println("����� �������� �������? � 1 �� " + cages.get(Bird.class.getSimpleName()).cage.size());
				indRemoveAnimal = sc.nextInt() - 1;
				sc.nextLine();
				if (indRemoveAnimal + 1 > cages.get(Bird.class.getSimpleName()).cage.size()) {
					return;
				}
				cages.get(Bird.class.getSimpleName()).removeAnimal(indRemoveAnimal);
			}

			System.out.println("����� ������ ������ " + cages.get(Bird.class.getSimpleName()).cage.size());
			break;

		case 3:
			if (cages.get(Herbivore.class.getSimpleName()).cage.size() == 0) {
				System.out.println("Error");
				return;
			} else {
				System.out.println(
						"����� �������� �������? � 1 �� " + cages.get(Herbivore.class.getSimpleName()).cage.size());
				indRemoveAnimal = sc.nextInt() - 1;
				sc.nextLine();
				if (indRemoveAnimal + 1 > cages.get(Herbivore.class.getSimpleName()).cage.size()) {
					return;
				}
				cages.get(Bird.class.getSimpleName()).removeAnimal(indRemoveAnimal);
			}

			System.out.println("����� ������ ������ " + cages.get(Herbivore.class.getSimpleName()).cage.size());
			break;
		}
		showAnimalInfo();
		System.out.println("New size of cagePredator = " + cages.get(Mammal.class.getSimpleName()).cage.size()
				+ "\nNew size of cageBird = " + cages.get(Bird.class.getSimpleName()).cage.size()
				+ "\nNew size of cageHerbivore = " + cages.get(Herbivore.class.getSimpleName()).cage.size());
	}

	@Override
	public void onAnimalDead(Animal animal) {

	}
}
